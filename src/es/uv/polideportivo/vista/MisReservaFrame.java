package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Reserva;
import es.uv.polideportivo.modelo.Socio;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class MisReservaFrame extends javax.swing.JFrame {
    
    private Vector<Reserva> res;
    private Socio socio;
    private String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};

    public MisReservaFrame(Vector<Reserva> res, Socio socio) {
        initComponents();
        this.setTitle("Mis Reservas");
        jLabel_error.setVisible(false);
        this.res = res;
        int i = 0;
        for (Reserva r : res){
            Calendar temp = r.getFecha();
            jTable1.setValueAt(r.getCancha(), i, 0);
            jTable1.setValueAt(temp.get(Calendar.DAY_OF_MONTH) + " de " + meses[temp.get(Calendar.MONTH)], i, 1);
            jTable1.setValueAt(r.getHini() + " - " + r.getHfin(), i, 2);
            i++;
        }
        this.socio = socio;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        b_modif = new javax.swing.JButton();
        jPanel_misReservas = new javax.swing.JPanel();
        b_atras = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        b_canc_res = new javax.swing.JButton();
        modificarButton = new javax.swing.JButton();
        jLabel_error = new javax.swing.JLabel();

        b_modif.setText("Modificar");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        b_atras.setText("Atras");
        b_atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_atrasActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Cancha", "Fecha", "Hora"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        b_canc_res.setText("Cancelar Reserva");
        b_canc_res.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_canc_resActionPerformed(evt);
            }
        });

        modificarButton.setText("Modificar");
        modificarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarButtonActionPerformed(evt);
            }
        });

        jLabel_error.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel_error.setForeground(new java.awt.Color(255, 0, 0));
        jLabel_error.setText("Error: Solo se pueden cancelar o modificar reservas con 24h de antelacion");

        javax.swing.GroupLayout jPanel_misReservasLayout = new javax.swing.GroupLayout(jPanel_misReservas);
        jPanel_misReservas.setLayout(jPanel_misReservasLayout);
        jPanel_misReservasLayout.setHorizontalGroup(
            jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                            .addComponent(modificarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(b_canc_res, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(b_atras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 428, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_misReservasLayout.createSequentialGroup()
                        .addComponent(jLabel_error)
                        .addContainerGap())))
        );
        jPanel_misReservasLayout.setVerticalGroup(
            jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(b_canc_res, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                    .addComponent(modificarButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(b_atras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel_error)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel_misReservas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(56, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel_misReservas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b_canc_resActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_canc_resActionPerformed
        int row = jTable1.getSelectedRow();
        Reserva reserva = res.get(row);
        Calendar fecha = new GregorianCalendar();
        int dia_ac = fecha.get(Calendar.DAY_OF_MONTH);
        int mes_ac = fecha.get(Calendar.MONTH);
        fecha = reserva.getFecha();
        int dia_res = fecha.get(Calendar.DAY_OF_MONTH);
        int mes_res = fecha.get(Calendar.MONTH);
        
        if ((dia_res-dia_ac)>=1 && mes_ac == mes_res){          
        
            res.remove(row);
        
            int aux = JOptionPane.showConfirmDialog(this, "¿Está seguro de que desea cancelar la reserva?");
            if(aux == JOptionPane.YES_OPTION){
            
                jTable1.setModel(new javax.swing.table.DefaultTableModel(
                    new Object [][] {
                        {null, null, null},
                        {null, null, null},
                        {null, null, null},
                        {null, null, null},
                        {null, null, null},
                        {null, null, null},
                        {null, null, null},
                        {null, null, null},
                        {null, null, null}
                    },
                    new String [] {
                        "Cancha", "Fecha", "Hora"
                    }
                )   {
                Class[] types = new Class [] {
                    java.lang.String.class, java.lang.String.class, java.lang.Integer.class
                };
                boolean[] canEdit = new boolean [] {
                    false, false, false
                };
    
                @Override
                public Class getColumnClass(int columnIndex) {
                    return types [columnIndex];
                }
    
                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit [columnIndex];
                }
            });
            int i = 0;
            for (Reserva r : res){
                Calendar temp = r.getFecha();
                jTable1.setValueAt(r.getCancha(), i, 0);
                jTable1.setValueAt(temp.get(Calendar.DAY_OF_MONTH) + " de " + meses[temp.get(Calendar.MONTH)], i, 1);
                jTable1.setValueAt(r.getHini() + " - " + r.getHfin(), i, 2);
                i++;
            }
            socio.cancelarReserva(reserva);
            }
        }
        else{
            jLabel_error.setVisible(true);
        }
    }//GEN-LAST:event_b_canc_resActionPerformed

    private void b_atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_atrasActionPerformed
        SocioFrame socioFrame = new SocioFrame(socio);
        socioFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_b_atrasActionPerformed

    private void modificarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarButtonActionPerformed
        ReservaCanchaFrame reservaCancha = new ReservaCanchaFrame(socio, res.get(jTable1.getSelectedRow()));
        reservaCancha.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_modificarButtonActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int row = jTable1.rowAtPoint(evt.getPoint());
        if (row < res.size()){
            modificarButton.setEnabled(true);
            b_canc_res.setEnabled(true);
        }
        else{
            modificarButton.setEnabled(false);
            b_canc_res.setEnabled(false);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_atras;
    private javax.swing.JButton b_canc_res;
    private javax.swing.JButton b_modif;
    private javax.swing.JLabel jLabel_error;
    private javax.swing.JPanel jPanel_misReservas;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton modificarButton;
    // End of variables declaration//GEN-END:variables
}

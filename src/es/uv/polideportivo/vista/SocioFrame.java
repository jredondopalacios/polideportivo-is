package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Clase;
import es.uv.polideportivo.modelo.Club;
import es.uv.polideportivo.modelo.Reserva;
import es.uv.polideportivo.modelo.Socio;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
        
/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class SocioFrame extends javax.swing.JFrame {
    
    private Socio socio;
    private Club cl;
    private Vector<Reserva> res_aux;
    private Vector<Clase> clas_aux;

    public SocioFrame(Socio socio) {
        initComponents();
        this.setTitle("Socio");
        this.socio = socio;
        jTextField_name.setText(socio.getNombre());
        
        final DefaultComboBoxModel canchaModel = new DefaultComboBoxModel(socio.getClub());
        lista_clubs.setModel(canchaModel);   
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelsocio = new javax.swing.JPanel();
        b_salir = new javax.swing.JButton();
        b_misreservas = new javax.swing.JButton();
        b_reserva = new javax.swing.JButton();
        b_horario = new javax.swing.JButton();
        b_baja = new javax.swing.JButton();
        lista_clubs = new javax.swing.JComboBox();
        jTextField_name = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        b_salir.setText("Salir");
        b_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_salirActionPerformed(evt);
            }
        });

        b_misreservas.setText("Mis Reservas");
        b_misreservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_misreservasActionPerformed(evt);
            }
        });

        b_reserva.setText("Reservar Cancha");
        b_reserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_reservaActionPerformed(evt);
            }
        });

        b_horario.setText("Horarios Clases");
        b_horario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_horarioActionPerformed(evt);
            }
        });

        b_baja.setText("Baja de club");
        b_baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_bajaActionPerformed(evt);
            }
        });

        lista_clubs.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Baloncesto", "Furbol", "Padel", "Tenis" }));
        lista_clubs.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                lista_clubsItemStateChanged(evt);
            }
        });
        lista_clubs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lista_clubsActionPerformed(evt);
            }
        });

        jTextField_name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("POLIDEPORTIVO");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nombre socio:");

        javax.swing.GroupLayout panelsocioLayout = new javax.swing.GroupLayout(panelsocio);
        panelsocio.setLayout(panelsocioLayout);
        panelsocioLayout.setHorizontalGroup(
            panelsocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelsocioLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelsocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelsocioLayout.createSequentialGroup()
                        .addGroup(panelsocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField_name, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(b_salir, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(b_reserva, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(b_misreservas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelsocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panelsocioLayout.createSequentialGroup()
                            .addComponent(b_baja, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(lista_clubs, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(b_horario, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38))
            .addGroup(panelsocioLayout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelsocioLayout.setVerticalGroup(
            panelsocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelsocioLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(panelsocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(b_salir, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelsocioLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField_name, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(b_reserva, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(b_misreservas, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(b_horario, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelsocioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_baja, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lista_clubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelsocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelsocio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_salirActionPerformed
        LoginFrame login = new LoginFrame();
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_b_salirActionPerformed

    private void b_reservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_reservaActionPerformed
        ReservaCanchaFrame nueva_res;
        nueva_res = new ReservaCanchaFrame(socio);
        this.dispose();
        nueva_res.setVisible(true);
    }//GEN-LAST:event_b_reservaActionPerformed

    private void b_misreservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_misreservasActionPerformed
        res_aux = socio.consultarReservas();
        MisReservaFrame mi_res;
        mi_res = new MisReservaFrame(res_aux, socio);
        this.dispose();
        mi_res.setVisible(true);
    }//GEN-LAST:event_b_misreservasActionPerformed

    private void b_horarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_horarioActionPerformed
        clas_aux = socio.consultarHorario();
        MisHorarios mis_h;
        mis_h = new MisHorarios(clas_aux, socio);
        this.dispose();
        mis_h.setVisible(true);
    }//GEN-LAST:event_b_horarioActionPerformed

    private void b_bajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_bajaActionPerformed
        int aux = JOptionPane.showConfirmDialog(this, "¿Está seguro de que quiere darse de baja de este club?");
        if(aux == JOptionPane.YES_OPTION){
            cl = (Club)lista_clubs.getSelectedItem();
            boolean b = socio.solicitarBaja(cl);
            if (b) {
                JOptionPane.showMessageDialog(this, "Se ha dado de baja del club seleccionado");
            }
            else {
                JOptionPane.showMessageDialog(this, "No se ha podido dar de baja");
            }
            final DefaultComboBoxModel canchaModel = new DefaultComboBoxModel(socio.getClub());
            lista_clubs.setModel(canchaModel);
        }
    }//GEN-LAST:event_b_bajaActionPerformed

    private void lista_clubsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lista_clubsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lista_clubsActionPerformed

    private void lista_clubsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_lista_clubsItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_lista_clubsItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_baja;
    private javax.swing.JButton b_horario;
    private javax.swing.JButton b_misreservas;
    private javax.swing.JButton b_reserva;
    private javax.swing.JButton b_salir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jTextField_name;
    private javax.swing.JComboBox lista_clubs;
    private javax.swing.JPanel panelsocio;
    // End of variables declaration//GEN-END:variables
}

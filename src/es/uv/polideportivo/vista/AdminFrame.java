package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Administrativo;
import es.uv.polideportivo.modelo.Clase;
import es.uv.polideportivo.modelo.Socio;
import es.uv.polideportivo.modelo.Usuario;
import java.util.Calendar;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class AdminFrame extends javax.swing.JFrame {
    private Administrativo admin;
    private int aux_currentRows;
    private Vector<Usuario> aux_socios;
    private String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};

    public AdminFrame(Administrativo admin) {

        initComponents();
        this.setTitle("Administrador");
        this.admin = admin;
        modificarButton.setEnabled(false);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        dniField = new javax.swing.JTextField();
        nombreField = new javax.swing.JTextField();
        busquedaButton = new javax.swing.JButton();
        altaSocioButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        resultTable = new javax.swing.JTable();
        salirButton = new javax.swing.JButton();
        modificarButton = new javax.swing.JButton();
        borrarButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jLabel1.setText("Buscar socio:");

        jLabel2.setText("DNI:");

        jLabel3.setText("Nombre:");

        busquedaButton.setText("Búsqueda");
        busquedaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busquedaButtonActionPerformed(evt);
            }
        });

        altaSocioButton.setText("Alta Socio");
        altaSocioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                altaSocioButtonActionPerformed(evt);
            }
        });

        resultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "DNI", "Nombre", "eMail", "Dirección", "Teléfono", "Fecha de Nac."
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        resultTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                resultTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(resultTable);

        salirButton.setText("Salir");
        salirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirButtonActionPerformed(evt);
            }
        });

        modificarButton.setText("Modificar Socio");
        modificarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                modificarButtonMouseClicked(evt);
            }
        });

        borrarButton.setText("Borrar Socio");
        borrarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                borrarButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(26, 26, 26)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel2)
                            .add(jLabel3))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(dniField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .add(nombreField))
                        .add(18, 18, 18)
                        .add(busquedaButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 144, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(altaSocioButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jScrollPane1))
                    .add(layout.createSequentialGroup()
                        .add(55, 55, 55)
                        .add(jLabel1)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(modificarButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 150, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(borrarButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 150, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(salirButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 150, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(33, 33, 33)
                .add(jLabel1)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel2)
                            .add(dniField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel3)
                            .add(nombreField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(layout.createSequentialGroup()
                        .add(1, 1, 1)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(altaSocioButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                            .add(busquedaButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 220, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(modificarButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .add(salirButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 35, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(borrarButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void altaSocioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_altaSocioButtonActionPerformed
        admin.iniciarAltaSocio();
        AltaSocioFrame altaSocio = new AltaSocioFrame(admin, null);
        altaSocio.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_altaSocioButtonActionPerformed

    private void busquedaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busquedaButtonActionPerformed
        String nombre = nombreField.getText();
        String dni = dniField.getText();
        aux_socios = admin.consultaSocio(nombre, dni);
        
        resultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "DNI", "Nombre", "eMail", "Dirección", "Teléfono", "Fecha de nacimiento"
            }
        ));
        
        int i = 0;
        for (Usuario u : aux_socios) {
            resultTable.setValueAt(u.getDni(), i, 0);
            resultTable.setValueAt(u.getNombre(), i, 1);
            resultTable.setValueAt(u.getE_mail(), i, 2);
            resultTable.setValueAt(u.getDireccion(), i, 3);
            resultTable.setValueAt(u.getTelefono(), i, 4);
            Calendar fecha = u.getFecha_nac();
            int dia = fecha.get(Calendar.DAY_OF_MONTH);
            int mes = fecha.get(Calendar.MONTH);
            int year = fecha.get(Calendar.YEAR);
            resultTable.setValueAt((dia + "-" + meses[mes] + "-" + year), i, 5);
            i++;
        }
        aux_currentRows = i;
        modificarButton.setEnabled(false);
    }//GEN-LAST:event_busquedaButtonActionPerformed

    private void modificarButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseClicked
           int row = resultTable.getSelectedRow();
           String dni = (String) resultTable.getValueAt(row, 0);
           Usuario usuario = null;
           for (Usuario u : aux_socios) {
               if(u.getDni().equals(dni)) {
                   usuario = u;
               }
           }
           admin.seleccionarSocio(usuario);
           AltaSocioFrame altaSocioFrame = new AltaSocioFrame(admin,usuario);
           altaSocioFrame.setVisible(true);
           this.dispose();
    }//GEN-LAST:event_modificarButtonMouseClicked

    private void resultTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resultTableMouseClicked
        int row = resultTable.rowAtPoint(evt.getPoint());
        if (row < aux_currentRows) {
            modificarButton.setEnabled(true);
        }
        else {
            modificarButton.setEnabled(false);
        }
    }//GEN-LAST:event_resultTableMouseClicked

    private void salirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirButtonActionPerformed
        LoginFrame login = new LoginFrame();
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_salirButtonActionPerformed

    private void borrarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_borrarButtonActionPerformed
        int aux = JOptionPane.showConfirmDialog(this, "¿Desea dar de baja al socio seleccionado?");
            if(aux == JOptionPane.YES_OPTION){
               int row = resultTable.getSelectedRow();
               String dni = (String) resultTable.getValueAt(row, 0);
               Usuario usuario = null;
               for (Usuario u : aux_socios) {
               if(u.getDni().equals(dni)) {
                   usuario = u;
               }
           }
           admin.borrarSocio(usuario);
           JOptionPane.showMessageDialog(this, "Socio borrado");
           AdminFrame refresh = new AdminFrame(admin);
           refresh.setVisible(true);
           this.dispose();
           }
    }//GEN-LAST:event_borrarButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton altaSocioButton;
    private javax.swing.JButton borrarButton;
    private javax.swing.JButton busquedaButton;
    private javax.swing.JTextField dniField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton modificarButton;
    private javax.swing.JTextField nombreField;
    private javax.swing.JTable resultTable;
    private javax.swing.JButton salirButton;
    // End of variables declaration//GEN-END:variables
}

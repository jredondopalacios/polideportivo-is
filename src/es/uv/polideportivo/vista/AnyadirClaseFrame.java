package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Cancha;
import es.uv.polideportivo.modelo.Encargado;
import es.uv.polideportivo.modelo.Profesor;
import es.uv.polideportivo.modelo.Usuario;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class AnyadirClaseFrame extends javax.swing.JFrame {
    
    private Encargado encargado;

    public AnyadirClaseFrame(Encargado e) {
        initComponents();
        this.setTitle("Añadir Clase");
        encargado = e;
        String[] diaSemana = {"Lunes", "Martes"};
        diaSemanaBox = new javax.swing.JComboBox(diaSemana);
        canchaBox.setEnabled(false);
        profesorBox.setEnabled(false);
        b_guardar.setEnabled(false);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel_Hini = new javax.swing.JLabel();
        jTextField_nom = new javax.swing.JTextField();
        jLabel_dia_sem = new javax.swing.JLabel();
        diaSemanaBox = new javax.swing.JComboBox();
        jLabel_nom1 = new javax.swing.JLabel();
        jLabel_Hfin = new javax.swing.JLabel();
        jComboBox_hini = new javax.swing.JComboBox();
        jComboBox_hfin = new javax.swing.JComboBox();
        jLabel_prof = new javax.swing.JLabel();
        canchaBox = new javax.swing.JComboBox();
        jLabel_cancha1 = new javax.swing.JLabel();
        profesorBox = new javax.swing.JComboBox();
        b_guardar = new javax.swing.JButton();
        b_cancelar = new javax.swing.JButton();
        comprobarButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jLabel_Hini.setText("Empieza:");

        jLabel_dia_sem.setText("Dia semana:");

        diaSemanaBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo" }));

        jLabel_nom1.setText("Nombre:");

        jLabel_Hfin.setText("Finaliza:");

        jComboBox_hini.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00" }));

        jComboBox_hfin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00" }));

        jLabel_prof.setText("Profesor:");

        jLabel_cancha1.setText("Cancha:");

        b_guardar.setText("Guardar");
        b_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_guardarActionPerformed(evt);
            }
        });

        b_cancelar.setText("Cancelar");
        b_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cancelarActionPerformed(evt);
            }
        });

        comprobarButton.setText("Comprobar disponibilidad");
        comprobarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprobarButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(b_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(b_cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(comprobarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(83, 83, 83))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel_prof, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel_Hini, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel_nom1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel_cancha1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel_dia_sem)
                                .addGap(12, 12, 12)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(diaSemanaBox, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jComboBox_hini, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(jLabel_Hfin, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBox_hfin, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jTextField_nom)
                            .addComponent(canchaBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(profesorBox, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField_nom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_nom1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_dia_sem, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(diaSemanaBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_Hini, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_Hfin, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox_hini, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox_hfin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(comprobarButton, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_cancha1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(canchaBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_prof, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(profesorBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(b_cancelar, javax.swing.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE)
                    .addComponent(b_guardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comprobarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprobarButtonActionPerformed
        int diaSemana = 0;
        switch(diaSemanaBox.getSelectedIndex()) {
            case 0:
                diaSemana = Calendar.MONDAY;
                break;
            case 1:
                diaSemana = Calendar.TUESDAY;
                break;
            case 2:
                diaSemana = Calendar.WEDNESDAY;
                break;
            case 3:
                diaSemana = Calendar.THURSDAY;
                break;
            case 4:
                diaSemana = Calendar.FRIDAY;
                break;
            case 5:
                diaSemana = Calendar.SATURDAY;
                break;
            case 6:
                diaSemana = Calendar.SUNDAY;
                break;
        }
        if (jTextField_nom.getText().equals("")){
            JOptionPane.showMessageDialog(this, "Escriba el nombre de la clase");
        }
        else{
        encargado.introducirClase(jTextField_nom.getText(), diaSemana,
            Integer.parseInt(((String) jComboBox_hini.getSelectedItem()).split(":")[0]), 
            Integer.parseInt(((String) jComboBox_hfin.getSelectedItem()).split(":")[0]));
        Vector<Cancha> cancha = encargado.getCanchasClase();
        Vector<Usuario> profesor = encargado.getProfesoresClase();
        final DefaultComboBoxModel canchaModel = new DefaultComboBoxModel(cancha);
        canchaBox.setModel(canchaModel);
        final DefaultComboBoxModel profesorModel = new DefaultComboBoxModel(profesor);
        profesorBox.setModel(profesorModel);
        canchaBox.setEnabled(true);
        profesorBox.setEnabled(true);
        b_guardar.setEnabled(true);
        }
    }//GEN-LAST:event_comprobarButtonActionPerformed

    private void b_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_guardarActionPerformed
        int aux = JOptionPane.showConfirmDialog(this, "¿Está seguro de que desea añadir una clase nueva?");
        if(aux == JOptionPane.YES_OPTION){
            Cancha c = (Cancha) canchaBox.getSelectedItem();
            Profesor p = (Profesor) profesorBox.getSelectedItem();
            encargado.seleccionarCanchaClase(c);
            encargado.seleccionarProfesor(p);
            encargado.confirmarClase();
            this.dispose();
            EncargadoFrame encargadoFrame = new EncargadoFrame(encargado);
            encargadoFrame.setVisible(true);
        }
    }//GEN-LAST:event_b_guardarActionPerformed

    private void b_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cancelarActionPerformed
        encargado.cancelarAnyadirClase();
        this.dispose();
        EncargadoFrame frame = new EncargadoFrame(encargado);
        frame.setVisible(true);
    }//GEN-LAST:event_b_cancelarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_cancelar;
    private javax.swing.JButton b_guardar;
    private javax.swing.JComboBox canchaBox;
    private javax.swing.JButton comprobarButton;
    private javax.swing.JComboBox diaSemanaBox;
    private javax.swing.JComboBox jComboBox_hfin;
    private javax.swing.JComboBox jComboBox_hini;
    private javax.swing.JLabel jLabel_Hfin;
    private javax.swing.JLabel jLabel_Hini;
    private javax.swing.JLabel jLabel_cancha1;
    private javax.swing.JLabel jLabel_dia_sem;
    private javax.swing.JLabel jLabel_nom1;
    private javax.swing.JLabel jLabel_prof;
    private javax.swing.JTextField jTextField_nom;
    private javax.swing.JComboBox profesorBox;
    // End of variables declaration//GEN-END:variables
}

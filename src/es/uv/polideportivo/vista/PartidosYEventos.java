package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Evento;
import es.uv.polideportivo.modelo.Partido;
import es.uv.polideportivo.modelo.Tiempo;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class PartidosYEventos extends javax.swing.JFrame {
    
    private Tiempo tiempo = new Tiempo();
    private String meses[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};

    public PartidosYEventos() {
        initComponents();
        this.setTitle("Partidos y Eventos");
        setLocationRelativeTo(null);
        class UserTimer extends TimerTask {
            @Override
            public void run() {
                String ev = "";
                String par = "";
                for(Evento e : tiempo.getInformacionEventos()) {
                    Calendar fechaEv = e.getFecha();
                    String aux = "Evento: " + e.getNombre() + 
                                " en " + e.getLugar() + 
                                " el "  + fechaEv.get(Calendar.DAY_OF_MONTH) + 
                                " de " + meses[fechaEv.get(Calendar.MONTH)]  + 
                                "\n" + e.getDescripcion() + "\n\n";
                    ev = ev + aux;
                }
                jTextArea_eventos.setText(ev);
                for(Partido p : tiempo.getInformacionPartidos()) {
                    Calendar fechaPar = p.getFecha();
                    String aux = "Partido: " + p.getEquipo1() + " contra " + p.getEquipo2() + 
                                "\nCancha: " + p.getCancha() +
                                "\nÁrbitro: " + p.getArbitro() +
                                "\nFecha: " + fechaPar.get(Calendar.DAY_OF_MONTH) +
                                " de " + meses[fechaPar.get(Calendar.MONTH)]  +
                                " de " + p.getHorario() + "\n\n";
                    par = par + aux;
                }
                jTextArea_partidos.setText(par);
            }
        };
        Timer timer = new Timer();
        timer.schedule(new UserTimer(), 0, 5000);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea_eventos = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea_partidos = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Próximos Eventos");

        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jTextArea_eventos.setEditable(false);
        jTextArea_eventos.setColumns(20);
        jTextArea_eventos.setRows(5);
        jScrollPane1.setViewportView(jTextArea_eventos);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Próximos Pártidos");

        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        jTextArea_partidos.setColumns(20);
        jTextArea_partidos.setRows(5);
        jScrollPane2.setViewportView(jTextArea_partidos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                    .addComponent(jLabel1))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea_eventos;
    private javax.swing.JTextArea jTextArea_partidos;
    // End of variables declaration//GEN-END:variables
}
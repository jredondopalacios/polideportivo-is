package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Cancha;
import es.uv.polideportivo.modelo.Reserva;
import es.uv.polideportivo.modelo.Socio;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class ReservaCanchaFrame extends javax.swing.JFrame {
    
    private Socio socio;
    private Calendar fecha = new GregorianCalendar();
    private Vector<Cancha> canchas = new Vector<Cancha>();
    private Reserva aux_reserva;

    public ReservaCanchaFrame(Socio socio) {
        initComponents();
        this.setTitle("Reservar Cancha");
        this.socio = socio;
        this.socio.iniciarReservaCancha();
        jLabel_fech_no.setVisible(false);
        jLabel_hora_no.setVisible(false);
        jLabel_hora_no2.setVisible(false);
        dateChooser.setCurrent(fecha);
        jComboBox_Hfin.setSelectedIndex(1);
    }
    
    public ReservaCanchaFrame(Socio socio, Reserva reserva){
        initComponents();
        this.setTitle("Modificar Cancha");
        this.socio = socio;
        this.socio.iniciarModificarReserva(reserva);
        jLabel_fech_no.setVisible(false);
        jLabel_hora_no.setVisible(false);
        jLabel_hora_no2.setVisible(false);
        Calendar fec = reserva.getFecha();
        
        dateChooser.setCurrent(fec);
        int hi = reserva.getHini();
        int hf = reserva.getHfin();
        hi -= 8;
        hf -= 8;
        jComboBox_Hini.setSelectedIndex(hi);
        jComboBox_Hfin.setSelectedIndex(hf);  
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_misReservas = new javax.swing.JPanel();
        b_atras = new javax.swing.JButton();
        jLabel_Hini = new javax.swing.JLabel();
        jComboBox_Hini = new javax.swing.JComboBox();
        jLabel_Hfin = new javax.swing.JLabel();
        jComboBox_Hfin = new javax.swing.JComboBox();
        jLabel_cancha = new javax.swing.JLabel();
        jComboBox_cancha = new javax.swing.JComboBox();
        b_guardar = new javax.swing.JButton();
        b_comp_fec = new javax.swing.JButton();
        b_comp_Hor = new javax.swing.JButton();
        jLabel_fech_no = new javax.swing.JLabel();
        jLabel_hora_no = new javax.swing.JLabel();
        jLabel_hora_no2 = new javax.swing.JLabel();
        dateChooser = new datechooser.beans.DateChooserCombo();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        b_atras.setText("Cancelar");
        b_atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_atrasActionPerformed(evt);
            }
        });

        jLabel_Hini.setText("Hora Inicio:");
        jLabel_Hini.setEnabled(false);

        jComboBox_Hini.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" }));
        jComboBox_Hini.setEnabled(false);
        jComboBox_Hini.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox_HiniItemStateChanged(evt);
            }
        });

        jLabel_Hfin.setText("Hora Fin");
        jLabel_Hfin.setEnabled(false);

        jComboBox_Hfin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" }));
        jComboBox_Hfin.setEnabled(false);

        jLabel_cancha.setText("Cancha:");
        jLabel_cancha.setEnabled(false);

        jComboBox_cancha.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox_cancha.setEnabled(false);

        b_guardar.setText("Guardar");
        b_guardar.setEnabled(false);
        b_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_guardarActionPerformed(evt);
            }
        });

        b_comp_fec.setText("Comprobar Fecha");
        b_comp_fec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_comp_fecActionPerformed(evt);
            }
        });

        b_comp_Hor.setText("Comprobar Horas");
        b_comp_Hor.setEnabled(false);
        b_comp_Hor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_comp_HorActionPerformed(evt);
            }
        });

        jLabel_fech_no.setForeground(new java.awt.Color(255, 0, 0));
        jLabel_fech_no.setText("Fecha no disponible");

        jLabel_hora_no.setForeground(new java.awt.Color(255, 0, 0));
        jLabel_hora_no.setText("Minimo una hora");

        jLabel_hora_no2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel_hora_no2.setText("Maximo tres horas");

        jLabel1.setText("Fecha:");

        javax.swing.GroupLayout jPanel_misReservasLayout = new javax.swing.GroupLayout(jPanel_misReservas);
        jPanel_misReservas.setLayout(jPanel_misReservasLayout);
        jPanel_misReservasLayout.setHorizontalGroup(
            jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_misReservasLayout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_misReservasLayout.createSequentialGroup()
                                .addComponent(b_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(b_atras, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_misReservasLayout.createSequentialGroup()
                                .addComponent(jLabel_fech_no, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(33, 33, 33))))
                    .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                                        .addComponent(jLabel_cancha, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBox_cancha, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                                        .addComponent(jLabel_Hfin, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jComboBox_Hfin, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                                        .addComponent(jLabel_Hini, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jComboBox_Hini, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_misReservasLayout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(dateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(b_comp_fec, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(b_comp_Hor, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                                .addGap(57, 57, 57)
                                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel_hora_no2)
                                    .addComponent(jLabel_hora_no))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel_misReservasLayout.setVerticalGroup(
            jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel_fech_no)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                        .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(b_comp_fec)
                            .addComponent(dateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel_Hini, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jComboBox_Hini, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                                .addComponent(jLabel_hora_no)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel_hora_no2)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel_Hfin, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox_Hfin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(b_comp_Hor))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel_cancha, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox_cancha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(b_atras, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(b_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel_misReservas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel_misReservas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b_comp_fecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_comp_fecActionPerformed

        if (socio.seleccionarFecha(dateChooser.getSelectedDate())){
            jLabel_fech_no.setVisible(false);
            jLabel_Hini.setEnabled(true);
            jComboBox_Hini.setEnabled(true);
            jLabel_Hfin.setEnabled(true);
            jComboBox_Hfin.setEnabled(true);
            b_comp_Hor.setEnabled(true);
        }
        else{
            jLabel_fech_no.setVisible(true);
        }
    }//GEN-LAST:event_b_comp_fecActionPerformed

    private void b_comp_HorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_comp_HorActionPerformed
        jLabel_hora_no.setVisible(false);
        jLabel_hora_no2.setVisible(false);
        int hora_ini = jComboBox_Hini.getSelectedIndex();
        int hora_fin = jComboBox_Hfin.getSelectedIndex();
        hora_ini+=8;
        hora_fin+=8;
        canchas = socio.seleccionarHora(hora_ini, hora_fin);
        if(!canchas.isEmpty()){
            final DefaultComboBoxModel canchaModel = new DefaultComboBoxModel(canchas);
            jComboBox_cancha.setModel(canchaModel);
            jLabel_cancha.setEnabled(true);
            jComboBox_cancha.setEnabled(true);
            b_guardar.setEnabled(true);
        }
        else{
            jLabel_hora_no.setVisible(true);
            jLabel_hora_no2.setVisible(true);
        }
    }//GEN-LAST:event_b_comp_HorActionPerformed

    private void b_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_guardarActionPerformed
        Cancha cancha;
        boolean b;
        cancha = (Cancha)jComboBox_cancha.getSelectedItem();
        socio.seleccionarCancha(cancha);
        b = socio.actualizarReserva();
        if (b){
            MisReservaFrame reservaFrame = new MisReservaFrame(socio.consultarReservas(), socio);
            reservaFrame.setVisible(true);
            this.dispose();
        }
        else{
            JOptionPane.showMessageDialog(this, "No se ha podido guardar la reserva.");
        }
        
    }//GEN-LAST:event_b_guardarActionPerformed

    private void b_atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_atrasActionPerformed
        socio.cancelarProcesoReserva();
        SocioFrame socioFrame = new SocioFrame(socio);
        socioFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_b_atrasActionPerformed

    private void jComboBox_HiniItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox_HiniItemStateChanged
        int i = jComboBox_Hini.getSelectedIndex();
        i++;
        jComboBox_Hfin.setSelectedIndex(i);
    }//GEN-LAST:event_jComboBox_HiniItemStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_atras;
    private javax.swing.JButton b_comp_Hor;
    private javax.swing.JButton b_comp_fec;
    private javax.swing.JButton b_guardar;
    private datechooser.beans.DateChooserCombo dateChooser;
    private javax.swing.JComboBox jComboBox_Hfin;
    private javax.swing.JComboBox jComboBox_Hini;
    private javax.swing.JComboBox jComboBox_cancha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel_Hfin;
    private javax.swing.JLabel jLabel_Hini;
    private javax.swing.JLabel jLabel_cancha;
    private javax.swing.JLabel jLabel_fech_no;
    private javax.swing.JLabel jLabel_hora_no;
    private javax.swing.JLabel jLabel_hora_no2;
    private javax.swing.JPanel jPanel_misReservas;
    // End of variables declaration//GEN-END:variables
}

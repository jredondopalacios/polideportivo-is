package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Clase;
import es.uv.polideportivo.modelo.Encargado;
import es.uv.polideportivo.modelo.Profesor;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class HorariosProfesorFrame extends javax.swing.JFrame {

    private Profesor profesor;
    private Vector<Clase> clase_aux;
    private boolean flag;
    
    private String cad[] = {"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"};
    
    public HorariosProfesorFrame() {
        initComponents();
        this.setTitle("Horarios Profesor");
    }

    public HorariosProfesorFrame(Profesor profesor) {
        initComponents();
        this.profesor = profesor;
        int tip = this.profesor.getTipo();
        if(tip == 2){//2 es el tipo del usuario que identifica al profesor
            this.setTitle("Horarios Profesor");
            flag = true;
        }
        else if(tip == 3){//3 es el tipo del usuario que identifica al encargado
            this.setTitle("Horarios Encargado");
            flag = false;
        }
        nombreProf.setText(profesor.getNombre());
        clase_aux = profesor.consultarHorario();
        int i = 0;
        for (Clase cl: clase_aux){
            tablaHorarios.setValueAt(cl, i, 0);
            tablaHorarios.setValueAt(cl.getHorario(), i, 1);
            tablaHorarios.setValueAt(cad[cl.getDiaSemana()], i, 2);
            tablaHorarios.setValueAt(cl.getNombreCancha(), i, 3);
            i++;
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_misReservas = new javax.swing.JPanel();
        b_atras = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaHorarios = new javax.swing.JTable();
        b_cancelar_clas = new javax.swing.JButton();
        nombreProf = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        b_atras.setText("Salir");
        b_atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_atrasActionPerformed(evt);
            }
        });

        tablaHorarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Clase", "Horario", "Día Semana", "Cancha", "Estado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tablaHorarios);

        b_cancelar_clas.setText("Cancelar Clase");
        b_cancelar_clas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cancelar_clasActionPerformed(evt);
            }
        });

        nombreProf.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Horario de clases de:");

        javax.swing.GroupLayout jPanel_misReservasLayout = new javax.swing.GroupLayout(jPanel_misReservas);
        jPanel_misReservas.setLayout(jPanel_misReservasLayout);
        jPanel_misReservasLayout.setHorizontalGroup(
            jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 428, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_misReservasLayout.createSequentialGroup()
                        .addComponent(b_cancelar_clas, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addComponent(b_atras, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(79, 79, 79))))
            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nombreProf, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel_misReservasLayout.setVerticalGroup(
            jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_misReservasLayout.createSequentialGroup()
                .addContainerGap(28, Short.MAX_VALUE)
                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(nombreProf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel_misReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(b_cancelar_clas, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(b_atras, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel_misReservas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel_misReservas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b_atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_atrasActionPerformed
        if(flag){
            LoginFrame login = new LoginFrame();
            login.setVisible(true);
            this.dispose();
        }
        else{
            EncargadoFrame encargado = new EncargadoFrame((Encargado)this.profesor);
            encargado.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_b_atrasActionPerformed

    private void b_cancelar_clasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cancelar_clasActionPerformed
        int aux = JOptionPane.showConfirmDialog(this, "¿Está seguro de que desea cancelar la próxima clase?");
        if(aux == JOptionPane.YES_OPTION){
            int row = tablaHorarios.getSelectedRow();
            Clase clase = (Clase) tablaHorarios.getValueAt(row, 0);
            profesor.cancelarClase(clase);
            JOptionPane.showMessageDialog(this, "Clase cancelada");
            clase_aux = profesor.consultarHorario();
            int i = 0;
            for (Clase cl: clase_aux){
                tablaHorarios.setValueAt(cl, i, 0);
                tablaHorarios.setValueAt(cl.getHorario(), i, 1);
                tablaHorarios.setValueAt(cad[cl.getDiaSemana()], i, 2);
                tablaHorarios.setValueAt(cl.getNombreCancha(), i, 3);
                if (cl.getEstado()) {
                    tablaHorarios.setValueAt("Cancelada", i, 4);
                    
                }
                i++;
            }   
         }
    }//GEN-LAST:event_b_cancelar_clasActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_atras;
    private javax.swing.JButton b_cancelar_clas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel_misReservas;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nombreProf;
    private javax.swing.JTable tablaHorarios;
    // End of variables declaration//GEN-END:variables
}

package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Encargado;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class EncargadoFrame extends javax.swing.JFrame {
    
    private Encargado encargado;

    public EncargadoFrame(Encargado encargado) {
        initComponents();
        this.setTitle("Encargado");
        this.encargado = encargado;
        nameLabel.setText(encargado.getNombre());
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        b_horario = new javax.swing.JButton();
        b_clase = new javax.swing.JButton();
        b_partido = new javax.swing.JButton();
        b_evento = new javax.swing.JButton();
        b_miembro_club = new javax.swing.JButton();
        b_salir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        b_horario.setText("Horario Profesor");
        b_horario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_horarioActionPerformed(evt);
            }
        });

        b_clase.setText("Añadir Clase");
        b_clase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_claseActionPerformed(evt);
            }
        });

        b_partido.setText("Organizar Partido");
        b_partido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                partidoActionPerformed(evt);
            }
        });

        b_evento.setText("Organizar Evento");
        b_evento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_eventoActionPerformed(evt);
            }
        });

        b_miembro_club.setText("Alta Miembro Club");
        b_miembro_club.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_miembro_clubActionPerformed(evt);
            }
        });

        b_salir.setText("Salir");
        b_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_salirActionPerformed(evt);
            }
        });

        jLabel1.setText("Nombre:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(83, 83, 83)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(b_miembro_club, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(b_salir, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(b_clase, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(b_partido, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(b_evento, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(b_horario, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nameLabel)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nameLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(b_horario, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(b_clase, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(b_partido, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(b_evento, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(b_salir, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                    .addComponent(b_miembro_club, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void b_claseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_claseActionPerformed
        encargado.iniciarAnyadirClase();
        AnyadirClaseFrame nuevaClase = new AnyadirClaseFrame(encargado);
        nuevaClase.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_b_claseActionPerformed

    private void b_eventoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_eventoActionPerformed
        encargado.iniciarOrganizarEvento();
        OrganizarEventoFrame1 nuevoEvento = new OrganizarEventoFrame1(encargado);
        nuevoEvento.setVisible(true);
        this.dispose();
        
    }//GEN-LAST:event_b_eventoActionPerformed

    private void partidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_partidoActionPerformed
        encargado.iniciarOrganizarPartido();
        OrganizarPartidoFrame organizarPartido = new OrganizarPartidoFrame(encargado);
        organizarPartido.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_partidoActionPerformed

    private void b_miembro_clubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_miembro_clubActionPerformed
        AltaMiembroClub altaMiembroClub = new AltaMiembroClub(encargado);
        altaMiembroClub.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_b_miembro_clubActionPerformed

    private void b_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_salirActionPerformed
        LoginFrame login = new LoginFrame();
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_b_salirActionPerformed

    private void b_horarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_horarioActionPerformed
        HorariosProfesorFrame profesor = new HorariosProfesorFrame(encargado);
        profesor.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_b_horarioActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_clase;
    private javax.swing.JButton b_evento;
    private javax.swing.JButton b_horario;
    private javax.swing.JButton b_miembro_club;
    private javax.swing.JButton b_partido;
    private javax.swing.JButton b_salir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel nameLabel;
    // End of variables declaration//GEN-END:variables
}

package es.uv.polideportivo.vista;

import es.uv.polideportivo.modelo.Administrativo;
import es.uv.polideportivo.modelo.Usuario;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class AltaSocioFrame extends javax.swing.JFrame {

    private Administrativo admin;
    private Usuario usuario;
    boolean flag = false;

    public AltaSocioFrame(Administrativo admin, Usuario u) {
        initComponents();
        this.setTitle("Alta Socio");
        this.admin = admin;
        if(u!=null) {
            this.setTitle("Modificar datos Socio");
            direccionField.setText(u.getDireccion());
            dniField.setText(u.getDni());
            emailField.setText(u.getE_mail());
            nombreField.setText(u.getNombre());
            telefonoField.setText(u.getTelefono());
            fecha_nac_chooser.setCurrent(u.getFecha_nac());
            altaSocioButton.setText("Modificar");
            flag = true;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dateChooserDialog1 = new datechooser.beans.DateChooserDialog();
        jLabel1 = new javax.swing.JLabel();
        emailField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        nombreField = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        dniField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        telefonoField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        direccionField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        passField = new javax.swing.JPasswordField();
        altaSocioButton = new javax.swing.JButton();
        cancelarButton = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        fecha_nac_chooser = new datechooser.beans.DateChooserCombo();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jLabel1.setText("eMail");

        jLabel2.setText("Nombre y apellidos *");

        jLabel3.setText("DNI*");

        jLabel4.setText("Teléfono*");

        jLabel5.setText("Dirección");

        jLabel6.setText("Contraseña*");

        altaSocioButton.setText("Alta Socio");
        altaSocioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                altaSocioButtonActionPerformed(evt);
            }
        });

        cancelarButton.setText("Cancelar");
        cancelarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarButtonActionPerformed(evt);
            }
        });

        jLabel7.setText("Fecha de nacimiento");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(20, 20, 20)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(jLabel7)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 25, Short.MAX_VALUE)
                        .add(fecha_nac_chooser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 77, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(layout.createSequentialGroup()
                            .add(6, 6, 6)
                            .add(jLabel3))
                        .add(jLabel4)
                        .add(jLabel5)
                        .add(jLabel2)
                        .add(nombreField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                        .add(telefonoField)
                        .add(dniField)
                        .add(emailField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                        .add(jLabel1)
                        .add(direccionField)))
                .add(34, 34, 34)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, altaSocioButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jLabel6)
                        .add(passField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, cancelarButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(22, 22, 22)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .add(jLabel6)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(passField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(69, 69, 69)
                        .add(altaSocioButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 65, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(25, 25, 25)
                        .add(cancelarButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 62, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(jLabel2)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(nombreField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(13, 13, 13)
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(emailField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jLabel3)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(dniField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jLabel4)
                        .add(10, 10, 10)
                        .add(telefonoField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(14, 14, 14)
                        .add(jLabel5)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(direccionField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel7)
                    .add(fecha_nac_chooser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void altaSocioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_altaSocioButtonActionPerformed
        String direccion = direccionField.getText();
        String dni = dniField.getText();
        String email = emailField.getText();
        String nombre = nombreField.getText();
        String pass = passField.getText();
        String telefono = telefonoField.getText();
        int aux;
       
        if (nombre.equals("") || dni.equals("") || telefono.equals("") || pass.equals("")){
            JOptionPane.showMessageDialog(this, "Los campos marcado con un * son obligatorios");
        }
        else{
            if(flag){
                aux = JOptionPane.showConfirmDialog(this, "¿Está seguro de que desea modificar los datos de " + nombre + "?");
            }else{
                aux = JOptionPane.showConfirmDialog(this, "¿Está seguro de que desea dar de alta a un nuevo socio?");
            }
                if(aux == JOptionPane.YES_OPTION){
                admin.guardarCambiosSocio(dni, nombre, direccion, telefono, fecha_nac_chooser.getSelectedDate(), email, pass);
                this.dispose();
                AdminFrame adminFrame = new AdminFrame(admin);
                adminFrame.setVisible(true);
            }
        }
    }//GEN-LAST:event_altaSocioButtonActionPerformed

    private void cancelarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarButtonActionPerformed
        admin.cancelarModificarSocio();
        this.dispose();
        AdminFrame adminFrame = new AdminFrame(admin);
        adminFrame.setVisible(true);
    }//GEN-LAST:event_cancelarButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton altaSocioButton;
    private javax.swing.JButton cancelarButton;
    private datechooser.beans.DateChooserDialog dateChooserDialog1;
    private javax.swing.JTextField direccionField;
    private javax.swing.JTextField dniField;
    private javax.swing.JTextField emailField;
    private datechooser.beans.DateChooserCombo fecha_nac_chooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField nombreField;
    private javax.swing.JPasswordField passField;
    private javax.swing.JTextField telefonoField;
    // End of variables declaration//GEN-END:variables
}

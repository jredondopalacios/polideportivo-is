package es.uv.polideportivo.modelo;

import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Tiempo{

	public Tiempo() {		
	}
        
        public Vector<Evento> getInformacionEventos() {
            return Polideportivo.getInstance().getEventos();
	}

	public Vector<Partido> getInformacionPartidos() {
            return Polideportivo.getInstance().getPartidos();
	}    
}
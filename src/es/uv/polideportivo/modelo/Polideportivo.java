package es.uv.polideportivo.modelo;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
class Polideportivo {

        private Vector<Clase> m_Clase = new Vector<Clase>();
	private Vector<Usuario> m_Usuario = new Vector<Usuario>();
	private Vector<Cancha> m_Cancha = new Vector<Cancha>();
	private Vector<Evento> m_Evento = new Vector<Evento>();
	private Vector<Reserva> m_Reserva = new Vector<Reserva>();
	private Vector<Partido> m_Partido = new Vector<Partido>();   
        private static Polideportivo instance = null;

	public Polideportivo() {
	}
        
        public static Polideportivo getInstance() {

            XStream xstream = new XStream(new DomDriver());
            xstream.alias("administrador", Administrativo.class);
            xstream.alias("cancha", Cancha.class);
            xstream.alias("clase", Clase.class);
            xstream.alias("club", Club.class);
            xstream.alias("encargado", Encargado.class);
            xstream.alias("equipo", Equipo.class);
            xstream.alias("evento", Evento.class);
            xstream.alias("partido", Partido.class);
            xstream.alias("polideportivo", Polideportivo.class);
            xstream.alias("profesor", Profesor.class);
            xstream.alias("reserva", Reserva.class);
            xstream.alias("socio", Socio.class);
            xstream.alias("usuario", Usuario.class);
            
            if(instance == null) {
                    try {
                    FileInputStream file_input_xml = new FileInputStream(new File("src/polideportivo.xml"));
                    instance = (Polideportivo) xstream.fromXML(file_input_xml);
                    
                    
                    
                    class BackupTimer extends TimerTask {
                            @Override
                            public void run() {
                                try {
                                    InputStream in = new FileInputStream("src/polideportivo.xml");
                                    OutputStream out = new FileOutputStream("src/polideportivo.xml.backup");
                                    byte[] buf = new byte[1024];
                                    int len;
                                    while((len = in.read(buf))>0) {
                                        out.write(buf,0,len);
                                    } 
                                    in.close();
                                    out.close();
                                } catch (FileNotFoundException ex) {
                                    Logger.getLogger(Polideportivo.class.getName()).log(Level.SEVERE, null, ex);
                                } catch (IOException ex) {
                                    Logger.getLogger(Polideportivo.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }   
                    };
                    Timer timer = new Timer();
                    timer.schedule(new BackupTimer(), 0, 3600000);
                    
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException ex) {
                    Logger.getLogger(Polideportivo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return instance; 
        }
        
        
        public void actualizarTurnos() {
            Reserva.idTurno = m_Reserva.size();
            Usuario.idTurno = m_Usuario.size();
            Partido.idTurno = m_Partido.size();
            Evento.idTurno = m_Evento.size();
            Clase.idTurno = m_Clase.size();
            Cancha.idTurno = m_Cancha.size();
        }

	public boolean actualizarReserva(Reserva reserva) {
		for (Reserva r : m_Reserva)
                {
                    if (r.getIdReserva() == reserva.getIdReserva()) {
                        m_Reserva.remove(r);
                        m_Reserva.add(reserva);
                        volcarAFichero();
                        return true;
                    }
                }
                m_Reserva.add(reserva);
                volcarAFichero();
                return true;
	}

	public boolean actualizarUsuario(Usuario usuario) {
		for (Usuario u : m_Usuario)
                {
                    if (u.getIdUsuario() == usuario.getIdUsuario()) {
                        m_Usuario.remove(u);
                        m_Usuario.add(usuario);
                        volcarAFichero();
                        return true;
                    }
                }
                m_Usuario.add(usuario);
                volcarAFichero();
                return true;
	}

	public boolean anyadirClase(Clase clase) {
		m_Clase.add(clase);
                volcarAFichero();
                return true;
	}

	public boolean anyadirEvento(Evento evento) {
		m_Evento.add(evento);
                volcarAFichero();
                return true;
	}

	public boolean anyadirPartido(Partido partido) {
		m_Partido.add(partido);
                volcarAFichero();
                return true;
	}

	public boolean borrarReserva(Reserva reserva) {
		m_Reserva.remove(reserva);
                volcarAFichero();
                return true;
	}

	public boolean borrarSocio(Usuario usuario) {
		m_Usuario.remove(usuario);
                volcarAFichero();
                return true;
	}

	public Vector<Cancha> buscarCanchaClase(int diaSemana, int horaIni, int horaFin) {
                Vector<Cancha> result = new Vector();
                for(Cancha c : m_Cancha) {
                    result.add(c);
                }
                for(Clase c: m_Clase) {
                    Cancha aux = c.getCanchaClase(diaSemana, horaIni, horaFin);
                    
                    if(aux!=null){
                        result.remove(aux);
                    }
                }
                return result;
	}

	public Vector<Cancha> buscarCanchaReserva(Calendar fecha, int horaIni, int horaFin) {
		Vector<Cancha> can = new Vector<Cancha>();
                for (Cancha c : m_Cancha) {
                    can.add(c);
                }
                for(Clase c: m_Clase) {
                    Cancha aux = c.getCancha(fecha, horaIni, horaFin);
                    if(aux!=null){
                        can.remove(aux);
                    }
                }
                for(Partido p: m_Partido) {
                    Cancha aux = p.getCancha(fecha, horaIni, horaFin);
                    if(aux!=null){
                        can.remove(aux);
                    }
                }
                for (Reserva r: m_Reserva) {
                    Cancha aux = r.getCancha(fecha, horaIni, horaFin);
                    if(aux!=null)
                        can.remove(aux);
                }
                return can;
	}
        
	public Vector<Clase> buscarClasesClub(int idClub) {
	    int id = idClub;	
            Vector<Clase> m_Clas_aux = new Vector<Clase>();
            for (Clase c : m_Clase) {
                if (id == c.getIdclub()) {   
                    m_Clas_aux.add(c);
                }  
            }
            return m_Clas_aux;
	}

	public Vector<Clase> buscarClasesProfesor(int idUsuario) {
            int id = idUsuario;
            Vector<Clase> mis_clases = new Vector<Clase>();
            for (Clase c : m_Clase) {
                if (id == c.getIDProfesor()) {   
                    mis_clases.add(c);
                }  
            }
            return mis_clases;
	}

	public Vector<Usuario> buscarProfesores(int diaSemana, int horaIni, int horaFin) {
		Vector<Usuario> result = new Vector();
                for(Usuario u : m_Usuario) {
                    if(u.getTipo()==GestorPolideportivo.PROFESOR)
                        result.add(u);
                }
                for(Clase c: m_Clase) {
                    Usuario aux = c.getProfesor(diaSemana, horaIni, horaFin);
                    if(aux!=null){
                        result.remove(aux);
                    }
                }
                return result;
	}

	public Usuario buscarSocio(String dni) {
		for(Usuario u : m_Usuario) {
                    if (u.getDni().equals(dni)) {
                        return u;
                    }
                } 
                return null;
	}

	public boolean cancelarClase(Clase clase) {
		clase.setCancelada();
                volcarAFichero();
                return true;
	}

	public boolean comprobarEventos(Calendar fecha) {
		boolean b = true;
                Calendar aux;
                int dia1 = fecha.get(Calendar.DAY_OF_MONTH);
                int mes1 = fecha.get(Calendar.MONTH);
                for (Evento e: m_Evento){
                    aux = e.getFecha();
                    int dia2 = aux.get(Calendar.DAY_OF_MONTH);
                    int mes2 = aux.get(Calendar.MONTH);
                    if(mes1 == mes2){
                        if (dia1 == dia2){
                            return false;
                        }
                    }
                }
                return b;
	}

	public Vector<Reserva> consultarReservas(int idUsuario) {
	    int id = idUsuario;	
            Vector<Reserva> m_Res_aux = new Vector<Reserva>();
            for (Reserva r : m_Reserva) {
                if (id == r.getId()) {   
                    m_Res_aux.add(r);
                }  
            }
            return m_Res_aux;   
	}

	public Vector<Usuario> consultaSocio(String nombre, String dni) {
                Vector<Usuario> uResult = new Vector();
		for (Usuario u : m_Usuario) {
                    if (nombre == null || u.getNombre().contains(nombre)) {
                        if (dni == null || u.getDni().contains(dni))
                            if (u.getTipo() == GestorPolideportivo.SOCIO)
                                uResult.add(u);
                    }
                }
                return uResult;
	}

	public Vector<Evento> getEventos() {
		return m_Evento;
	}

	public Vector<Partido> getPartidos() {
		return m_Partido;
	}

	public void volcarAFichero() {
            XStream xstream = new XStream(new DomDriver());
            xstream.alias("administrador", Administrativo.class);
            xstream.alias("cancha", Cancha.class);
            xstream.alias("clase", Clase.class);
            xstream.alias("club", Club.class);
            xstream.alias("encargado", Encargado.class);
            xstream.alias("equipo", Equipo.class);
            xstream.alias("evento", Evento.class);
            xstream.alias("partido", Partido.class);
            xstream.alias("polideportivo", Polideportivo.class);
            xstream.alias("profesor", Profesor.class);
            xstream.alias("reserva", Reserva.class);
            xstream.alias("socio", Socio.class);
            xstream.alias("usuario", Usuario.class);
                       
            String xml = xstream.toXML(this);
            System.out.println(xml);
            try {
                FileWriter file_output_xml = new FileWriter(new File("src/polideportivo.xml"));
                file_output_xml.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
                file_output_xml.write(xml);
                file_output_xml.close();
                
            } catch (IOException e) {
                e.printStackTrace();
            }
	}

        //Función que se ejecuta sólo la primera vez que se lanza la aplicación para crear un xml con datos
        public void cargaInicial (){
            
            Calendar fechanac = Calendar.getInstance();
            fechanac.set(1985, 11, 5);
            
            Socio inma = new Socio ("1111", "Inma Garcia", fechanac, "Avenida Constitucion", "666777888", "ingarpe2@alumni.uv.es", "1111", 1);
            Socio carlos = new Socio ("2222", "Carlos Gomez", fechanac, "Calle Grande", "666888999", "carlos@alumni.uv.es", "2222", 1);
            Socio juan = new Socio ("3333", "Juan Juan", fechanac, "Calle Nueva", "666555444", "juan@alumni.uv.es", "3333", 1);
            
            Equipo leones = new Equipo ("Leones");
            leones.addUsuario(carlos);
            carlos.addEquipo(leones);
            Equipo aguilas = new Equipo ("Aguilas");
            aguilas.addUsuario(juan);
            juan.addEquipo(aguilas);
            Equipo tenistas = new Equipo ("Tenistas");
            tenistas.addUsuario(carlos);
            carlos.addEquipo(tenistas);
            Equipo elastic = new Equipo ("Elastic");
            elastic.addUsuario(inma);
            inma.addEquipo(elastic);
            Equipo gymgym = new Equipo ("Gymgym");
            gymgym.addUsuario(juan);
            juan.addEquipo(gymgym);
            
            Club futbol = new Club ("Futbol 7");
            futbol.addEquipo(leones);
            futbol.addEquipo(aguilas);
            futbol.addUsuario(carlos);
            futbol.addUsuario(juan);
            carlos.addClub(futbol);
            juan.addClub(futbol);
            Club tenis = new Club ("Tenis");
            tenis.addEquipo(tenistas);
            tenis.addUsuario(carlos);
            carlos.addClub(tenis);
            Club gimnasia = new Club ("Gym");
            gimnasia.addEquipo(elastic);
            gimnasia.addEquipo(gymgym);
            gimnasia.addUsuario(inma);
            gimnasia.addUsuario(juan);
            inma.addClub(gimnasia);
            juan.addClub(gimnasia);
                        
            Profesor joan = new Profesor ("4444", "Joan Gomez", fechanac, "Avenida del mar", "666777888", "joan@alumni.uv.es", "4444", 2);
            Profesor maria = new Profesor ("5555", "Maria Lopez", fechanac, "Avenida del mar", "666777888", "maria@alumni.uv.es", "5555", 2);
            Profesor ana = new Profesor ("6666", "Ana Roa", fechanac, "Avenida del mar", "666777888", "ana@alumni.uv.es", "6666", 2);

            Encargado sergio = new Encargado ("7777", "Sergio Leal", fechanac, "Avenida del mar", "666777888", "sergio@alumni.uv.es", "7777", 3, futbol);
            Encargado raul = new Encargado ("8888", "Raul Martin", fechanac, "Avenida del mar", "666777888", "raul@alumni.uv.es", "8888", 3, tenis);
            Encargado sila = new Encargado ("9999", "Sila Real", fechanac, "Avenida del mar", "666777888", "sila@alumni.uv.es", "9999", 3, gimnasia);

            Administrativo manolo = new Administrativo ("1234", "Manolo Leal", fechanac, "Avenida del mar", "666777888", "manolo@alumni.uv.es", "1234", 4);
            Administrativo rosa = new Administrativo ("2345", "Rosa Gomez", fechanac, "Avenida del mar", "666777888", "rosa@alumni.uv.es", "2345", 4);
            Administrativo lola = new Administrativo ("3456", "Lola Garcia", fechanac, "Avenida del mar", "666777888", "lola@alumni.uv.es", "3456", 4);

            Cancha canchaFut = new Cancha ("Futbol");
            Cancha canchaTenis = new Cancha ("Tenis");
            Cancha canchaGim = new Cancha ("Gimnasia");

            Clase claseFut = new Clase ("Penaltis", Calendar.MONDAY, 17, 18, canchaFut, joan, futbol);
            Clase claseTen = new Clase ("Tenis dobles", Calendar.TUESDAY, 18, 19, canchaTenis, maria, tenis);
            Clase claseGim = new Clase ("Artistica", Calendar.WEDNESDAY, 19, 20, canchaGim, ana, gimnasia);

            Calendar cal1 = Calendar.getInstance();
            cal1.set(2013, 7, 1);
            Calendar cal2 = Calendar.getInstance();
            cal2.set(2013, 8, 1);
            Calendar cal3 = Calendar.getInstance();
            cal3.set(2013, 9, 1);

            Evento liga = new Evento ("La Liga", cal1, "Polideportivo", "Liga anual de fútbol", true);
            Evento open = new Evento ("Open tenis", cal2, "Polideportivo", "Open de Tenis", true);
            Evento maraton = new Evento ("Maratón", cal3, "Paterna", "Maratón anual", false);

            Calendar cal4 = Calendar.getInstance();
            cal4.set(2013, 7, 2);
            Calendar cal5 = Calendar.getInstance();
            cal5.set(2013, 8, 2);
            Calendar cal6 = Calendar.getInstance();
            cal6.set(2013, 9, 2);

            Partido part1 = new Partido (cal4, 9, 10, canchaFut, futbol, "Howard Webb");
            part1.setEquipo1(leones);
            part1.setEquipo1(aguilas);
            Partido part2 = new Partido (cal4, 10, 11, canchaTenis, tenis, "Massimo Busacca");
            part2.setEquipo1(tenistas);
            part2.setEquipo1(tenistas);
            Partido part3 = new Partido (cal5, 11, 12, canchaFut, futbol, "Roberto Rosetti");
            part3.setEquipo1(aguilas);
            part3.setEquipo1(leones);
      
            Calendar cal7 = Calendar.getInstance();
            cal7.set(2013, 7, 3);
            Calendar cal8 = Calendar.getInstance();
            cal8.set(2013, 8, 3);
            Calendar cal9 = Calendar.getInstance();
            cal9.set(2013, 9, 3);

            Reserva res1 = new Reserva (cal7, 13, 14, inma, canchaFut);
            Reserva res2 = new Reserva (cal8, 14, 15, carlos, canchaTenis);
            Reserva res3 = new Reserva (cal9, 16, 17, juan, canchaFut);
            
            m_Clase.add(claseFut);
            m_Clase.add(claseTen );
            m_Clase.add(claseGim );
            m_Usuario.add(inma);
            m_Usuario.add(carlos);
            m_Usuario.add(juan);
            m_Usuario.add(joan);
            m_Usuario.add(maria);
            m_Usuario.add(ana);
            m_Usuario.add(sergio);
            m_Usuario.add(raul);
            m_Usuario.add(sila);
            m_Usuario.add(manolo);
            m_Usuario.add(rosa);
            m_Usuario.add(lola);
            m_Cancha.add(canchaFut);
            m_Cancha.add(canchaTenis);
            m_Cancha.add(canchaGim);
            m_Evento.add(liga);
            m_Evento.add(open);
            m_Evento.add(maraton);
            m_Reserva.add(res1);
            m_Reserva.add(res2);
            m_Reserva.add(res3);
            m_Partido.add(part1);
            m_Partido.add(part2);
            m_Partido.add(part3);
        }
        
        public Vector<Usuario> getUsuarios() {
            return m_Usuario;
        }
}
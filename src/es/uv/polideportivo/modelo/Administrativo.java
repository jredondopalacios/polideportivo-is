package es.uv.polideportivo.modelo;

import java.util.Calendar;
import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Administrativo extends Usuario {
        
        private Usuario aux_socio;
        
        public Administrativo (){
            super();
        }

	public Administrativo(String dni, String nombre, Calendar fecha_nac, String direccion, String telefono, String e_mail, String password, int tipo) {
            super(dni, nombre, fecha_nac, direccion, telefono, e_mail, password, tipo);
	}

	public boolean borrarSocio(Usuario socio) {
                return polideportivo.borrarSocio(socio);
	}

	public void cancelarModificarSocio() {
                aux_socio = null;
	}

	public Vector<Usuario> consultaSocio(String nombre, String dni) {
		return polideportivo.consultaSocio(nombre, dni);
	}

	public boolean guardarCambiosSocio(String dni, String nombre, String direccion, String telefono, Calendar fecha_nac, String e_mail, String password) {
                aux_socio.setE_mail(e_mail);
                aux_socio.setDireccion(direccion);
                aux_socio.setDni(dni);
                aux_socio.setFecha_nac(fecha_nac);
                aux_socio.setNombre(nombre);
                aux_socio.setPassword(password);
                aux_socio.setTelefono(telefono);
                return polideportivo.actualizarUsuario(aux_socio);
	}

	public void iniciarAltaSocio() {
		aux_socio = new Socio();
                aux_socio.setTipo(GestorPolideportivo.SOCIO);
	}

	public void seleccionarSocio(Usuario socio) {
                aux_socio = socio;
	}
}
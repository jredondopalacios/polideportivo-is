package es.uv.polideportivo.modelo;

import java.util.Calendar;
import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Profesor extends Usuario {
	
	public Profesor() {
            super();
	}
        
        public Profesor(String dni, String nombre, Calendar fecha_nac, String direccion, String telefono, String e_mail, String password, int tipo) {
            super(dni, nombre, fecha_nac, direccion, telefono, e_mail, password, tipo);
	}

	public boolean cancelarClase(Clase clase) {
		polideportivo.cancelarClase(clase);
                return true;
	}

	public Vector<Clase> consultarHorario() {
                Vector<Clase> clases = new Vector<Clase>();   
                clases = polideportivo.buscarClasesProfesor(idUsuario);
                return clases;
	}
}
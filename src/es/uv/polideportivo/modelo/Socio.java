package es.uv.polideportivo.modelo;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Socio extends Usuario {
    
	private Reserva aux_reserva;

	public Socio() {
            super();
	}
        
        public Socio(String dni, String nombre, Calendar fecha_nac, String direccion, String telefono, String e_mail, String password, int tipo){
            super(dni, nombre, fecha_nac, direccion, telefono, e_mail, password, tipo);
        }

	public boolean actualizarReserva() {
            boolean b;
            b = polideportivo.actualizarReserva(aux_reserva);
            aux_reserva = null;
            return b;
	}

	public void cancelarProcesoReserva() {
		aux_reserva = null;
	}

	public boolean cancelarReserva(Reserva reserva) {
		return polideportivo.borrarReserva(reserva);
	}

	public boolean comprobarAntelacion(Calendar fecha) {
            boolean b;
            Calendar c2 = new GregorianCalendar();
            if (fecha.before(c2)){
                return false;
            }
            int dia = c2.get(Calendar.DAY_OF_MONTH);
            int mes = c2.get(Calendar.MONTH);
            int dia2 = fecha.get(Calendar.DAY_OF_MONTH);
            int mes2 = fecha.get(Calendar.MONTH);
            if (mes == mes2){
                int dif = dia2 - dia;
                if((dif)>2){
                    b = true;
                }
                else{
                    b = false;
                }
            }
            else{
                b = true;
            }
            return b;
	}

	public boolean comprobarRango(int horaIni, int horaFin) {
            boolean b = false;
            if ( (horaFin-horaIni) >= 1 && (horaFin-horaIni) <= 3 ){
                b = true;
            }
            return b;    
	}

	public Vector<Clase> consultarHorario() {
            Vector<Clase> clas = new Vector<Clase>();
            Vector<Clase> aux;
            for(Club c : this.m_Club){
                aux = polideportivo.buscarClasesClub(c.getId());
                for (Clase cla: aux){
                    clas.add(cla);
                }
            }
            return clas;
	}

	public Vector<Reserva> consultarReservas() {
		Vector<Reserva> res;
                res = polideportivo.consultarReservas(this.getIdUsuario());
                return res;
	}

	public void iniciarModificarReserva(Reserva reserva) {
                aux_reserva = reserva;
	}

	public void iniciarReservaCancha() {
                aux_reserva = new Reserva();
		aux_reserva.setSocio(this);
	}

	public void seleccionarCancha(Cancha cancha) {
		aux_reserva.setCancha(cancha);
	}

	public boolean seleccionarFecha(Calendar fecha) {
                if (comprobarAntelacion(fecha)){
                    if( polideportivo.comprobarEventos(fecha)) {
                        aux_reserva.setFecha(fecha);
                        return true;
                    }
                }
                return false;
	}

	public Vector<Cancha> seleccionarHora(int horaIni, int horaFin) {
                Vector<Cancha> canchas = new Vector<Cancha>();
                if(this.comprobarRango(horaIni, horaFin)){
                    canchas = polideportivo.buscarCanchaReserva(aux_reserva.getFecha(), horaIni, horaFin);
                    aux_reserva.setHoraIni(horaIni);
                    aux_reserva.setHoraFin(horaFin);
                }
                return canchas;
	}

	public boolean solicitarBaja(Club club) {
                boolean b;
                b = m_Club.remove(club);
                if(b == true){
                    polideportivo.actualizarUsuario(this);
                }
                return b;
	}
}
package es.uv.polideportivo.modelo;

import java.util.Calendar;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Evento {
    
	private int idEvento;
	private String nombre;
	private Calendar fecha;
	private String lugar;
	private String descripcion;
        private boolean dentro;
        protected static int idTurno = 0;

	public Evento() {
            idEvento = idTurno;
            idTurno++;
	}

	public Evento(String nombre, Calendar fecha, String lugar, String descripcion, Boolean dentro) {
            idEvento = idTurno;
            idTurno++;
            this.nombre = nombre;
            this.fecha = fecha;
            this.lugar = lugar;
            this.descripcion = descripcion;
            this.dentro = dentro;
	}
        
        public boolean getDentroPolideportivo() {
            return dentro;
        }
        
        public String getDescripcion() {
            return descripcion;
        }
        
        public Calendar getFecha() {
            return fecha;
        }
        
        public int getId() {
            return idEvento;
        }
        
        public String getLugar() {
            return lugar;
        }
        
        public String getNombre() {
            return nombre;
        }
        
        public void setDentroPolideportivo(boolean b) {
            dentro = b;
        }

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
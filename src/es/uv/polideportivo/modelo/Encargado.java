package es.uv.polideportivo.modelo;

import java.util.Calendar;
import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Encargado extends Profesor {
    
	private Clase aux_clase;
	private Evento aux_evento;
	private Partido aux_partido;
	private Socio aux_socio;
        private Equipo aux_equipo;
	private Club m_Club;
        
        public Encargado(){
            super();
        }

	public Encargado(String dni, String nombre, Calendar fecha_nac, String direccion, String telefono, String e_mail, String password, int tipo, Club m_Club) {
            super(dni, nombre, fecha_nac, direccion, telefono, e_mail, password, tipo);
            this.m_Club = m_Club;
	}
        
        public void cancelarAnyadirClase() {
		aux_clase = null;
	}
        
        public void cancelarOrganizarEvento() {
		aux_evento = null;
	}
        
        public void cancelarOrganizarPartido() {
		aux_partido = null;
	}
        
        public boolean confirmarClase() {
		polideportivo.anyadirClase(aux_clase);
                return true;
	}
        
        public boolean confirmarMiembro(Equipo equipo) {
                aux_equipo = equipo;
                aux_socio.addEquipo(aux_equipo);
                aux_equipo.addUsuario(aux_socio);
                aux_socio.addClub(m_Club);
                m_Club.addUsuario(aux_socio);
		polideportivo.actualizarUsuario(aux_socio);
                aux_equipo = null;
                aux_socio = null;
                return true;
	}
        
        public boolean confirmarPartido() {
		polideportivo.anyadirPartido(aux_partido);
                cancelarOrganizarPartido();
                return true;
	}
        
        public Vector<Cancha> getCanchasClase() {
                return polideportivo.buscarCanchaClase(aux_clase.getDiaSemana(), aux_clase.getHoraIni(), aux_clase.getHoraFin());
        }
    
        public Club getClubEncargado() {
                return m_Club;
        }

        public Vector<Usuario> getProfesoresClase() {
               return polideportivo.buscarProfesores(aux_clase.getDiaSemana(), aux_clase.getHoraIni(), aux_clase.getHoraFin());
        }
        
        public boolean iniciarAltaMiembroClub(String dni) {
		Usuario aux_usuario = polideportivo.buscarSocio(dni);
                if ( (aux_usuario == null) || (aux_usuario.getTipo() != GestorPolideportivo.SOCIO)) {
                    return false;
                }
                aux_socio = (Socio)aux_usuario;
                return true;
	}

	public void iniciarAnyadirClase() {
            aux_clase = new Clase();
            aux_clase.setClub(m_Club);
	}
        
        public void iniciarOrganizarEvento() {
		aux_evento = new Evento();
	}
        
        public void iniciarOrganizarPartido() {
		aux_partido = new Partido();
                aux_partido.setClub(m_Club);
	}
        
        public void introducirArbitro(String arbitro) {
		aux_partido.setArbitro(arbitro);
	}

	public void introducirClase(String nombreClase, int diaSemana, int horaIni, int horaFin) {
		aux_clase.setNombre(nombreClase);
                aux_clase.setDiaSemana(diaSemana);
                aux_clase.setHoraIni(horaIni);
                aux_clase.setHoraFin(horaFin);
	}
        
        public boolean introducirEvento(String nombre, Calendar fecha, String lugar, String descripcion, boolean dentro) {
                boolean introducido = false;
                if(polideportivo.comprobarEventos(fecha)){
                    aux_evento.setNombre(nombre);
                    aux_evento.setFecha(fecha);
                    aux_evento.setLugar(lugar);
                    aux_evento.setDescripcion(descripcion);
                    aux_evento.setDentroPolideportivo(dentro);
                    polideportivo.anyadirEvento(aux_evento);
                    introducido = true;
                }
                return introducido;
	}

	public Vector<Cancha> introducirPartido(Equipo equipo1, Equipo equipo2, Calendar fecha, int horaIni, int horaFin) {
                aux_partido.setEquipo1(equipo1);
                aux_partido.setEquipo2(equipo2);
                aux_partido.setFecha(fecha);
                aux_partido.setHoraIni(horaIni);
                aux_partido.setHoraFin(horaFin);
                return polideportivo.buscarCanchaReserva(fecha, horaIni, horaFin);
	}
        
        public void seleccionarCanchaClase(Cancha cancha) {
		aux_clase.setCancha(cancha);
	}

	public void seleccionarCanchaPartido(Cancha cancha) {
		aux_partido.setCancha(cancha);
	}

	public void seleccionarProfesor(Profesor profesor) {
		aux_clase.setProfesor(profesor);
	}   
}
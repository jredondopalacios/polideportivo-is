package es.uv.polideportivo.modelo;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.File;
import java.io.FileInputStream;
import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class GestorPolideportivo {

    public static final int SOCIO = 1;
    public static final int PROFESOR = 2;
    public static final int ENCARGADO = 3;
    public static final int ADMINISTRATIVO = 4;

    public static Usuario identificarse(String dni, String password) {
            
            //Sólo para la carga inicial de datos (creación del xml)
            //Polideportivo.getInstance().cargaInicial();
            //Polideportivo.getInstance().volcarAFichero();
            
            for (Usuario u : Polideportivo.getInstance().getUsuarios()) {
                if (dni.equals(u.getDni())) {   
                    if (u.comprobarPassword(password)) {
                        Polideportivo polideportivo = Polideportivo.getInstance();
                        Usuario usuario = polideportivo.buscarSocio(u.getDni());
                        usuario.setInstance(Polideportivo.getInstance());
                        polideportivo.actualizarTurnos();
                        return usuario;
                    } 
                }
            }           
            return null;
    }   
}
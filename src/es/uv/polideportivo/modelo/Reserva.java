package es.uv.polideportivo.modelo;

import java.util.Calendar;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Reserva {
    
	private int idReserva;
	private Calendar fecha;
	private int horaIni;
	private int horaFin;
	private Socio m_Socio;
	private Cancha m_Cancha;
        protected static int idTurno = 0;

	public Reserva() {
            idReserva = idTurno;
            idTurno++;
	}
        
        public Reserva(Calendar fecha, int horaIni, int horaFin, Socio m_Socio, Cancha m_Cancha) {
            idReserva = idTurno;
            idTurno++;
            this.fecha = fecha;
            this.horaIni = horaIni;
            this.horaFin = horaFin;
            this.m_Socio = m_Socio;
            this.m_Cancha = m_Cancha;
	}
	
        public Cancha getCancha(){
                return this.m_Cancha;
        }
        
        public Cancha getCancha(Calendar fecha, int horaIni, int horaFin) {
		int dia = fecha.get(Calendar.DAY_OF_YEAR);
                if(dia == this.fecha.get(Calendar.DAY_OF_YEAR)) {
                    if ((horaIni>=this.horaIni && horaIni<this.horaFin) ||
                            (horaFin>this.horaIni && horaFin<=this.horaFin) ||
                            (horaIni<=this.horaIni && horaFin>=this.horaFin)){
                                return m_Cancha;
                    }
                }
                return null;
	}
        
        public Calendar getFecha(){
                return this.fecha;
        }
        
        public int getHini(){
                return this.horaIni;
        }
        
        public int getHfin(){
                return this.horaFin;
        }
        
        public  int getId() {
            return m_Socio.getIdUsuario();
	}
        
        public int getIdReserva(){
            return this.idReserva;
        }
        
        private Socio getSocio() {
            return m_Socio;
        }
 
	public void setCancha(Cancha cancha) {
		this.m_Cancha = cancha;
	}

	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}

	public void setHoraFin(int horaFin) {
		this.horaFin = horaFin;
	}

	public void setHoraIni(int horaIni) {
		this.horaIni = horaIni;
	}

	public void setSocio(Socio socio) {
		this.m_Socio = socio;
	}

        @Override
        public String toString(){
            String s = (this.m_Socio.getNombre());
            return s;
        }
}
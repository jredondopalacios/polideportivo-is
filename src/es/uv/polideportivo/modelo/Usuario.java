package es.uv.polideportivo.modelo;

import java.util.Calendar;
import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Usuario {
    
	protected int idUsuario;
	private String dni;
	private String nombre;
	private Calendar fecha_nac;
	private String direccion;
	private String telefono;
	private String e_mail;
	private String password;
        protected int tipo;
        protected Polideportivo polideportivo;
	protected Vector<Equipo> m_Equipo = new Vector<Equipo>();
	protected Vector<Club> m_Club = new Vector<Club>();
        protected static int idTurno = 0;
        
	public Usuario() {
            idUsuario = idTurno;
            idTurno = idTurno + 1;
	}
        
        public Usuario(String dni, String nombre, Calendar fecha_nac, String direccion, String telefono, String e_mail, String password, int tipo) {
            idUsuario = idTurno;
            idTurno = idTurno + 1;
            this.dni = dni;
            this.nombre = nombre;
            this.fecha_nac = fecha_nac;
            this.direccion = direccion;
            this.telefono = telefono;
            this.e_mail = e_mail;
            this.password = password;
            this.tipo = tipo;
	}
        
        public void addClub (Club club){
            m_Club.add(club);
        }
    
        public void addEquipo (Equipo equipo){
            m_Equipo.add(equipo);
        }
 
	public boolean comprobarPassword(String password) {
		return (password == null ? this.password == null : password.equals(this.password));
	}
        
        public Vector<Club> getClub(){
            return m_Club;
        }
        
        public String getDireccion() {
            return direccion;
        }

        public String getDni() {
            return dni;
        }
        
        public String getE_mail() {
           return e_mail;
        }
        
        public Calendar getFecha_nac() {
            return fecha_nac;
        }

        public int getIdUsuario() {
            return idUsuario;
        }
        
        public String getNombre() {
            return nombre;
        }
        
        public String getTelefono() {
            return telefono;
        }
        
        public int getTipo() {
            return tipo;
        }
        
        public void setDireccion(String direccion) {
            this.direccion = direccion;
        }

        public void setDni(String dni) {
            this.dni = dni;
        }
        
        public void setE_mail(String e_mail) {
            this.e_mail = e_mail;
        }
        
        public void setFecha_nac(Calendar fecha_nac) {
            this.fecha_nac = fecha_nac;
        }
        
        void setInstance(Polideportivo instance) {
           this.polideportivo = instance;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
        
        public void setPassword(String password) {
           this.password = password;
        }

        public void setTelefono(String telefono) {
            this.telefono = telefono;
        }
        
        public void setTipo(int tipo) {
           this.tipo = tipo;
        }

        @Override
        public String toString() {
            return nombre;
        }
}
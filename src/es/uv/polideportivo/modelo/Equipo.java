package es.uv.polideportivo.modelo;

import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Equipo {
    
	private int idEquipo;
	private String nombre;
	private Vector<Usuario> m_Usuario = new Vector<Usuario>();
        protected static int idTurno = 0;

	public Equipo() {
            idEquipo = idTurno;
            idTurno++;
	}
        
        public Equipo(String nombre) {
            idEquipo = idTurno;
            idTurno++;
            this.nombre = nombre;
	}
        
        public void addUsuario(Usuario u) {
            m_Usuario.add(u);
        }
        
        public int getId() {
            return idEquipo;
        }
        
        public String getNombre() {
            return nombre;
        }
        
        public Vector<Usuario> getUsuarios() {
            return m_Usuario;
        }

        public void setNombre(String s) {
            nombre = s;
        }
        
        @Override
        public String toString() {
            return nombre;
        }
}
package es.uv.polideportivo.modelo;

import java.util.Calendar;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Clase {
    
	private int idClase;
	private String nombre;
	private int diaSemana;
	private int horaIni;
	private int horaFin;
        private boolean cancelada = false;
	private Cancha m_Cancha;
	private Profesor m_Profesor;
	private Club club;
        protected static int idTurno = 0;

	public Clase() {
            idClase = idTurno;
            idTurno++;
	}
        
        public Clase(String nombre, int diaSemana, int horaIni, int horaFin, Cancha m_Cancha, Profesor m_Profesor, Club club) {
            idClase = idTurno;
            idTurno++;
            this.nombre = nombre;
            this.diaSemana = diaSemana;
            this.horaIni = horaIni;
            this.horaFin = horaFin;
            this.m_Cancha = m_Cancha;
            this.m_Profesor = m_Profesor;
            this.club = club;
	}
        
        public Cancha getCancha(Calendar fecha, int horaIni, int horaFin) {
		int dia = fecha.get(Calendar.DAY_OF_WEEK);
                return getCanchaClase(dia, horaIni, horaFin);
	}
        
        public int getCancha(){
                return m_Cancha.getIdCancha();
        }
        
        /**
         * getCanchaClase devuelve una Cancha válida si el horario de la Clase coincide con los parámetros.
         * 
         * @param diaSemana Día de la semana de la clase
         * @param horaIni Hora de inicio
         * @param horaFin Hora de fin
         * @return Devuelve la cancha de la clase si la Clase coincide con los parámetos. null en caso contrario
         */
	public Cancha getCanchaClase(int diaSemana, int horaIni, int horaFin) {
		if(diaSemana == this.diaSemana) {
                    if ((horaIni>=this.horaIni && horaIni<this.horaFin) ||
                            (horaFin>this.horaIni && horaFin<=this.horaFin) ||
                            (horaIni<=this.horaIni && horaFin>=this.horaFin)) {
                                return m_Cancha;
                    }
                }
                return null;
	}
        
        public int getDiaSemana() {
            return diaSemana;
        }
        
        public boolean getEstado(){
            return cancelada;
        }
        
        public int getHoraFin() {
            return horaFin;
        }
        
        public int getHoraIni() {
            return horaIni;
        }
        
        public String getHorario(){
            return (this.horaIni + " - " + this.horaFin);
        }
        
        public int getIdClase(){
                return this.idClase;
        }
        
        public int getIdclub(){
            return this.club.getId();
        }
        
        public int getIDProfesor (){
            return m_Profesor.getIdUsuario();
        }
        
        public String getNombreCancha(){
                return m_Cancha.getNombre();
        }
        
        public String getNombreClub(){
            return club.getNombreClub();
        }

        /**
         * getProfesor devuelve un Profesor válido si el horario de la Clase coincide con los parámetros.
         * 
         * @param diaSemana Día de la semana de la clase
         * @param horaIni Hora de inicio
         * @param horaFin Hora de fin
         * @return Devuelve el profesor de la clase si la Clase coincide con los parámetos. null en caso contrario
         */
	public Usuario getProfesor(int diaSemana, int horaIni, int horaFin) {
		if(diaSemana == this.diaSemana) {
                    if ((horaIni>=this.horaIni && horaIni<this.horaFin) ||
                            (horaFin>this.horaIni && horaFin<=this.horaFin)) {
                                return m_Profesor;
                    }
                }
                return null;
	}

	public void setCancelada() {
		cancelada = true;
	}

	public void setCancha(Cancha cancha) {
		m_Cancha = cancha;
	}
        
        public void setClub(Club club) {
		this.club = club;
	}

	public void setDiaSemana(int diaSemana) {
		this.diaSemana = diaSemana;
	}

	public void setHoraFin(int horaFin) {
		this.horaFin = horaFin;
	}

	public void setHoraIni(int horaIni) {
		this.horaIni = horaIni;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setProfesor(Profesor profesor) {
		m_Profesor = profesor;
	} 
       
        @Override
        public String toString(){
            return this.nombre;
        }
}

package es.uv.polideportivo.modelo;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Cancha {
    
	private int idCancha;
	private String nombre;
        protected static int idTurno = 0;
	
        public Cancha() {
            idCancha = idTurno;
            idTurno = idTurno + 1;
	}
        
        public Cancha(String nombre) {
            idCancha = idTurno;
            idTurno = idTurno + 1;
            this.nombre = nombre;
	}
        
        public int getIdCancha(){
            return this.idCancha;
        }
        
        public String getNombre() {
            return nombre;
        }
                
        public void setIdCancha(int id){
            idCancha = id;
        }

        public void setNombre(String s) {
            nombre = s;
        }
                
        @Override
        public String toString() {
            return nombre;
        }
}
package es.uv.polideportivo.modelo;

import java.util.Vector;
import java.util.Calendar;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Partido {
    
	private int idPartido;
	private Calendar fecha;
	private int horaIni;
	private int horaFin;
        private String arbitro;
	private Cancha m_Cancha;
	private Vector<Equipo> m_Equipo = new Vector<Equipo>();
	private Club m_Club;
        protected static int idTurno = 0;

	public Partido() {
            idPartido = idTurno;
            idTurno++;
	}

	public Partido(Calendar fecha, int horaIni, int horaFin, Cancha m_Cancha, Club m_Club, String arbitro) {
            idPartido = idTurno;
            idTurno++;
            this.fecha = fecha;
            this.horaIni = horaIni;
            this.horaFin = horaFin;
            this.m_Cancha = m_Cancha;
            this.m_Club = m_Club;
            this.arbitro = arbitro;
	}
        
        public String getArbitro() {
            return arbitro;
        }

	public Cancha getCancha(Calendar fecha, int horaIni, int horaFin) {
		int dia = fecha.get(Calendar.DAY_OF_YEAR);
                if(dia == this.fecha.get(Calendar.DAY_OF_YEAR)) {
                    if ((horaIni>=this.horaIni && horaIni<this.horaFin) ||
                            (horaFin>this.horaIni && horaFin<=this.horaFin) ||
                            (horaIni<=this.horaIni && horaFin>=this.horaFin)){
                                return m_Cancha;
                    }
                }
                return null;
	}
        
        public Cancha getCancha(){
            return m_Cancha;
        }
        
        public Club getClub(){
            return m_Club;
        }
        
        public Equipo getEquipo1(){
            return m_Equipo.elementAt(0);
        }
        
        public Equipo getEquipo2(){
            return m_Equipo.elementAt(1);
        }
        
        public Calendar getFecha() {
            return fecha;
        }
        
        public String getHorario(){
            return (this.horaIni + " a " + this.horaFin);
        }
        
        public void setArbitro(String s) {
            arbitro = s;
        }

	public void setCancha(Cancha cancha) {
		m_Cancha = cancha;
	}

	public void setClub(Club club) {
		this.m_Club = club;
	}

	public void setEquipo1(Equipo equipo1) {
		m_Equipo.add(0, equipo1);
	}

	public void setEquipo2(Equipo equipo2) {
		m_Equipo.add(1, equipo2);
	}

	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}

	public void setHoraFin(int horaFin) {
		this.horaFin = horaFin;
	}

	public void setHoraIni(int horaIni) {
		this.horaIni = horaIni;
	}  
}

package es.uv.polideportivo.modelo;

import java.util.Vector;

/**
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class Club {
    
	private int idClub;
	private String nombre;
	private Vector<Equipo> m_Equipo = new Vector<Equipo>();
	private Vector<Usuario> m_Usuario = new Vector<Usuario>();
        protected static int idTurno = 0;

	public Club() {
            idClub = idTurno;
            idTurno++;
	}
        
        public Club(String nombre) {
            idClub = idTurno;
            idTurno++;
            this.nombre = nombre;
	}    
        
        public void addEquipo (Equipo equipo){
            m_Equipo.add(equipo);
        }
        
        public void addUsuario (Usuario usuario){
            m_Usuario.add(usuario);
        }
        
	public Vector<Equipo> buscarEquipos() {
		return m_Equipo;
        }
        
        public int getId(){       
            return this.idClub;
        }
        
        public String getNombreClub(){
            return this.nombre;
        }
        
        @Override
        public String toString(){
            return this.nombre;
        }
}
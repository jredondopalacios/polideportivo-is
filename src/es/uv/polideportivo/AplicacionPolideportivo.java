package es.uv.polideportivo;

import es.uv.polideportivo.vista.LoginFrame;
import es.uv.polideportivo.vista.PartidosYEventos;

/**
 * Aplicación para un Polideportivo
 * Proyecto de Ingeniería del Software I
 * Grado en Ingeniería Informática I
 * Universitat de València
 * @author Jordi Redondo, Alejandro Sanchís, Inma García
 * 
 */
public class AplicacionPolideportivo {
    public static void main(String[] args) {
        LoginFrame login = new LoginFrame();
        login.setVisible(true);
        PartidosYEventos partidosYEventos = new PartidosYEventos();
        partidosYEventos.setVisible(true);
    }
}